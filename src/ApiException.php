<?php
namespace Termiyanc\Api;

/**
 * Собственный класс исключений
 * @package Termiyanc\Api
 */
class ApiException extends \Exception
{
    public $class;

    /**
     * @param $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }
}
