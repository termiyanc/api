<?php
namespace Termiyanc\Api;

/**
 * Абстрактный класс для работы с API
 * @package Termiyanc\Api
 *
 * Требует PHP >= 5.5.0 с подключенным расширением "curl".
 *
 * Для работы необходимо определить производный неабстрактный класс.
 *
 * В свойстве $methods в производном классе необходимо описать методы обращения к API в формате:
 *
 * [
 *     // Имя метода
 *     'method' => [
 *         // Url, переопределяющий url в общей конфигурации
 *         'url' => string,
 *         // Путь, прибавляемый к url
 *         'url_suffix' => string,
 *         // Код ответа при успехе
 *         'success_response_code' => int
 *         // Параметры метода
 *         'params' => [
 *             // Имя параметра
 *             'param' => [
 *                 // Обязательный ли параметр. По умолчанию параметр не обязателен
 *                 'required' => bool,
 *                 // Значение по умолчанию, переопределяющее значение по умолчанию в общей конфигурации.
 *                 // Рассматривается, если параметр является обязательным и не определен
 *                 'default' => mixed
 *             ]
 *         ]
 *     ]
 * ]
 *
 * Чтобы обратиться к методу API, необходимо произвести статический вызов className::method([array $params], [bool $checkResponseCode]), где:
 * className — имя производного класса,
 * method — название метода в свойстве $methods производного класса,
 * $params — массив параметров вызываемого метода (необязательно),
 * $checkResponseCode — только ли проверить код ответа (необязательно, по умолчанию — false).
 *
 * Для производного класса может быть определена конфигурация в свойстве className::$config формата:
 * [
 *     // Url для запросов к API
 *     'url' => string,
 *     // Код ответа при успехе
 *     'success_response_code' => int
 *     // Значения параметров методов API по умолчанию
 *     'params_defaults' => array,
 *     // Общие параметры методов API
 *     'common_params' => array,
 * ]
 *
 * В производном классе может быть описан метод selfInit():void с собственными действиями при инициализации.
 *
 * В производном классе может быть описан метод curlParams():array, возвращающий параметры curl.
 *
 * Обращение к API производится методом POST, параметры передаются в формате JSON, передается соответствующий заголовок "Content-Type".
 * При обращении по https устанавливаются параметры curl:
 * [
 *     CURLOPT_SSL_VERIFYPEER => 0,
 *     CURLOPT_SSL_VERIFYHOST => 0
 * ]
 *
 * В реультате обращения к методу API возвращается:
 * экземпляр класса ApiResult
 * или булево значение, соответствует ли код ответа определенному коду ответа при успехе, если при вызове метода был определен параметр `$checkResponseCode`;
 * или результат работы метода `className::postProcess()`.
 *
 */
abstract class Api
{
    private static $initialized;

    private static $curl;
    private static $instances = [];

    /**
     * @var self
     */
    private static $currentInstance;

    /**
     * Рабочие параметры для API методов по умолчанию
     */
    private static $defaultApiMethodOperationalParams = [
        'url' => null,
        'is_https' => false,
        'has_required_params' => false,
        'process_result' => true,
        'success_response_code' => 200
    ];

    // Параметры API
    protected $config = [];

    // Методы API
    protected $methods = [];

    // Закрытые не-singleton методы
    private function __construct(){}
    private function __clone(){}

    /**
     * Метод производит внутреннюю инициализацию
     * @throws ApiException
     */
    private static function init()
    {
        if (!extension_loaded('curl')) {
            throw (new ApiException("Extension \"curl\" is required!"))->setClass(self::class);
        }
        if (!self::$curl) {
            self::$curl = curl_init();
        }
        self::$initialized = true;
    }

    /**
     * Метод инициализирует API
     * @throws ApiException
     */
    private static function initApi()
    {
        // Обращаюсь к собственному инициализирующему методу
        self::$currentInstance->selfInit();
        // Определяю собственные параметры curl
        self::$instances[static::class]['curl_params'] = self::$currentInstance->curlParams();
        // Определяю рабочие параметры для API методов текущего экземпляра
        self::defineApiMethodsOperationalParams();
    }

    /**
     * В перегруженном методе в производном классе могут быть описаны собственные действия при инициализации
     */
    protected function selfInit(){}

    /**
     * В перегруженном методе в производном классе могут возвращаться собственные параметры curl
     * @return array
     */
    protected function curlParams(){
        return [];
    }

    /**
     * Метод определяет рабочие параметры для API методов текущего экземпляра
     */
    private static function defineApiMethodsOperationalParams()
    {
        if (self::$currentInstance->methods) {
            foreach (array_keys(self::$currentInstance->methods) as $method) {
                self::$currentInstance->methods[$method]['operational_params'] = self::$defaultApiMethodOperationalParams;
                $ApiMethodOperationalParams = &self::$currentInstance->methods[$method]['operational_params'];
                // Определяю url метода API по url, определенному для метода API, или общему url из конфигурации
                if (($url =
                        trim(self::$currentInstance->methods[$method]['url'] !== null ? self::$currentInstance->methods[$method]['url'] :
                            self::$currentInstance->config['url'])) !== '') {
                    // Определяю https
                    $ApiMethodOperationalParams['is_https'] = mb_strpos($ApiMethodOperationalParams['url'], 'https') === 0;
                    // Прибавляю к url суффикс
                    if (($suffix = trim(self::$currentInstance->methods[$method]['url_suffix'])) !== '') {
                        $url .= $suffix;
                    }
                    $ApiMethodOperationalParams['url'] = $url;
                }
                // Определяю, есть ли обязательные параметры для метода
                if (self::$currentInstance->methods[$method]['params']) {
                    foreach (self::$currentInstance->methods[$method]['params'] as $param => $paramDefinition) {
                        if (!is_int($param) && $paramDefinition['required']) {
                            $ApiMethodOperationalParams['has_required_params'] = true;
                            break;
                        }
                    }
                }
                // Определяю, возвращать ли результат
                if (self::$currentInstance->methods[$method]['process_result'] !== null) {
                    $ApiMethodOperationalParams['process_result'] = self::$currentInstance->methods[$method]['process_result'];
                }
                // Определяю код успеха
                if (($successResponseCode =
                        self::$currentInstance->methods[$method]['success_response_code'] !== null ? self::$currentInstance->methods[$method]['success_response_code'] :
                            (self::$currentInstance->config['success_response_code'] !== null ? self::$currentInstance->config['success_response_code'] : null)) !== null) {
                    $ApiMethodOperationalParams['success_response_code'] = (int)$successResponseCode;
                }
            }
        }
    }

    /**
     * Метод обрабатывает статический вызов как обращение к методу API
     * @param string $method
     * @param array $arguments
     * @return ApiResult|bool|mixed
     * @throws ApiException
     */
    public static function __callStatic($method, $arguments)
    {
        try {
            if (!self::$initialized) {
                self::init();
            }
            if (!self::$instances[static::class]) {
                // Определяю объект производного класса
                self::$currentInstance = self::$instances[static::class]['instance'] = new static;
                self::initApi();
            }
            if (!self::$currentInstance->methods[$method]) {
                throw new ApiException("API method \"$method\" is not defined!");
            }
            if (self::$currentInstance->methods[$method]['operational_params']['url'] === null) {
                throw new ApiException("Url for method \"$method\" is not defined!");
            }
            if ($arguments[0] !== null && !is_array($arguments[0])) {
                throw new ApiException("Passed params must be defined as array (called API method \"$method\")!");
            }
            if (!$arguments[0] && self::$currentInstance->methods[$method]['operational_params']['has_required_params']) {
                throw new ApiException("There are required params for API method \"$method\"!");
            }
            return self::doApiRequest($method, $arguments[0], $arguments[1]);
        } catch (ApiException $e) {
            return (new ApiResult($e->class ? : static::class))->setError($e->getMessage());
        }
    }

    /**
     * Метод производит запрос к API
     * @param string $method
     * @param array $params
     * @param bool $checkResponseCode
     * @return ApiResult|bool|mixed
     * @throws ApiException
     */
    private static function doApiRequest($method, $params, $checkResponseCode = false)
    {
        $requestParams = [];
        // Обрабатываю указанные в описании метода API параметры
        if (self::$currentInstance->methods[$method]['params']) {
            foreach (self::$currentInstance->methods[$method]['params'] as $param => $paramDefinition) {
                // Параметр без описания
                if (is_int($param)) {
                    $param = $paramDefinition;
                    if ($params[$param]) {
                        $requestParams[$param] = $params[$param];
                        unset($params[$param]);
                    }
                    continue;
                }
                // Обязательный параметр
                if ($paramDefinition['required']) {
                    if ($params[$param]) {
                        $requestParams[$param] = $params[$param];
                        unset($params[$param]);
                    } elseif ($paramDefinition['default']) {
                        $requestParams[$param] = $paramDefinition['default'];
                    } elseif (self::$currentInstance->config['params_defaults'][$param]) {
                        $requestParams[$param] = self::$currentInstance->config['params_defaults'][$param];
                    } elseif (!array_key_exists($param, self::$currentInstance->config['common_params'])) {
                        throw new ApiException("Missing required param \"$param\" for API method \"$method\"!");
                    }
                    continue;
                }
                // Обычный параметр
                if ($params[$param]) {
                    $requestParams[$param] = $params[$param];
                    unset($params[$param]);
                }
            }
        }
        // Устанавливаю прочие переданные параметры
        foreach ($params as $param => $value) {
            $requestParams[$param] = $value;
        }
        // Устанавливаю параметры по умолчанию
        if (self::$currentInstance->config['common_params']) {
            foreach (self::$currentInstance->config['common_params'] as $commonParam => $value) {
                if (!array_key_exists($commonParam, $requestParams)) {
                    $requestParams[$commonParam] = $value;
                }
            }
        }
        // Устанавливаю общие параметры curl
        curl_setopt_array(self::$curl, [
            CURLOPT_URL => self::$currentInstance->methods[$method]['operational_params']['url'],
            CURLOPT_POSTFIELDS => json_encode($requestParams),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json'
            ]
        ]);
        // Устанавливаю параметры curl для https
        if (self::$currentInstance->methods[$method]['operational_params']['is_https']) {
            curl_setopt_array(self::$curl, [
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_SSL_VERIFYHOST => 0
            ]);
        }
        // Устанавливаю собственные параметры curl
        if (self::$instances[static::class]['curl_params']) {
            curl_setopt_array(self::$curl, self::$instances[static::class]['curl_params']);
        }
        // Проверяю код ответа...
        if ($checkResponseCode) {
            curl_setopt(self::$curl, CURLOPT_NOBODY, true);
            if (curl_exec(self::$curl) !== false) {
                return
                    curl_getinfo(self::$curl, CURLINFO_RESPONSE_CODE) ===
                        self::$currentInstance->methods[$method]['operational_params']['success_response_code'];
            }
        } else {
            // ...или работаю с результатом
            curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, true);
            if (($requestResult = curl_exec(self::$curl)) !== false) {
                $decodedRequestResult = json_decode($requestResult, true);
                // Определяю объект ApiResult и передаю в метод постобработки
                return static::postProcess(
                    (new ApiResult(static::class))
                        ->setCalledApiMethod($method)
                            ->setRequestUrl(self::$currentInstance->methods[$method]['operational_params']['url'])
                                ->setRequestParams($requestParams)
                                    ->setRequestSuccess(true)
                                        ->setResponseCode(curl_getinfo(self::$curl, CURLINFO_RESPONSE_CODE))
                                            ->setRequestResult($decodedRequestResult)
                );
            }
        }
        return
            (new ApiResult(static::class))
                ->setCalledApiMethod($method)
                    ->setRequestUrl(self::$currentInstance->methods[$method]['operational_params']['url'])
                        ->setRequestParams($requestParams)
                            ->setRequestSuccess(false);
    }

    /**
     * В перегруженном методе в производном классе может быть описана постобработка результата
     * @param ApiResult $result
     * @return ApiResult
     */
    protected static function postProcess(ApiResult $result)
    {
        return $result;
    }
}
