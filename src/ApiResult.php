<?php
namespace Termiyanc\Api;

/**
 * Результат выполнения запроса
 * @package Termiyanc\Api
 *
 * @method ApiResult setClass(mixed $value = null)
 * @method ApiResult setError(mixed $value = null)
 * @method ApiResult setCalledApiMethod(mixed $value = null)
 * @method ApiResult setRequestUrl(mixed $value = null)
 * @method ApiResult setRequestParams(mixed $value = null)
 * @method ApiResult setRequestSuccess(mixed $value = null)
 * @method ApiResult setResponseCode(mixed $value = null)
 * @method ApiResult setRequestResult(mixed $value = null)
 */
class ApiResult
{
    public $class;
    public $error;
    public $calledApiMethod;
    public $requestUrl;
    public $requestParams;
    public $requestSuccess;
    public $responseCode;
    public $requestResult;

    /**
     * Метод-конструктор. Устанавливает класс
     * @param $class
     */
    public function __construct($class)
    {
        return $this->setClass($class);
    }

    /**
     * Метод устанавливает существующее свойство propertyName при вызове $object->setPropertyName($value)
     * @param string $name
     * @param array $arguments
     * @return self
     */
    public function __call($name, $arguments)
    {
        if (property_exists($this, $property = lcfirst(preg_replace('/^set/', '', $name)))) {
            $this->$property = $arguments[0];
        }
        return $this;
    }
}
